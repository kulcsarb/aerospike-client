
class info = object(self)	
	val mutable commands = ""
	val mutable response = ""

	method set_command command = 
		commands <- command ^ "\n"

	method response = response 
	
	method size () = 
		String.length commands

	method bit_size () =
		self#size () * 8

	method encode () = 
		let%bitstring b = {|
 			2							: 8  : int;
			1		 					: 8  : int;
			Int64.of_int (self#size ())	: 48 : bigendian;
 			commands					: self#bit_size () : string
 		|} in 
 		b

 	method decode bits = 
		match%bitstring bits with 
		|{|
 			2				: 8  : int;
			1				: 8  : int;
			size		 	: 48 : bigendian;
 			info 			: (Int64.to_int size) * 8 : string
 		|} ->
 			response <- info

end

class message = object(self)
	
	val mutable header 		: Header.t 			= Header.empty ()
	val mutable fields 		: Field.t list 		= []
	val mutable operations 	: Operation.t list	= []

	method header = header
	method operations = operations
	method fields = fields

 	(* 
 		for all read operations:
	 	header.info1_level_b0 or b1 <- true ??? read_attr |= INFO1_CONSISTENCY_ALL if policy.consistency_level == Aerospike::ConsistencyLevel::CONSISTENCY_ALL 
	*)

	method add_operation operation = 
		operations <- [ operation ] @ operations

	method exists () = 
		header.info1_read <- true;
		header.info1_nobindata <- true;

	method get () = 
		header.info1_read <- true;
		header.info1_get_all <- true;

	method get_bin bin_name =
		header.info1_read <- true;
		self#add_operation @@ Operation.get ~bin_name

	method put bin_name value = 
		header.info2_write <- true;
		self#add_operation @@ Operation.put ~bin_name ~value

	method append bin_name value = 
		header.info2_write <- true;
		self#add_operation @@ Operation.append ~bin_name ~value

	method prepend bin_name value = 
		header.info2_write <- true;
		self#add_operation @@ Operation.prepend ~bin_name ~value

	method inc bin_name value = 
		header.info2_write <- true;
		self#add_operation @@ Operation.inc ~bin_name ~value

	method touch () =
		header.info2_write <- true;
		self#add_operation @@ Operation.touch ()

	method delete () =
		header.info2_write <- true;
		header.info2_delete <- true;

	method set_key ?(send_key=true) (key: Key.t) = 	
		if key.namespace <> "" then 
			self#add_namespace key.namespace;
		if key.set <> "" then 
			self#add_set key.set;		
		self#add_digest key.digest;
		if send_key then 
			self#add_user_key key.key

	method get_key () : Key.t = 
		let namespace = ref "" in
		let set = ref "" in
		let digest = ref "" in 
		let key = ref (`Str "") in 
		List.iter (fun (f:Field.t) ->  
			begin match f.field_type with 
			| AS_MSG_FIELD_TYPE_NAMESPACE -> namespace := f.data
			| AS_MSG_FIELD_TYPE_SET -> set := f.data
			| AS_MSG_FIELD_TYPE_DIGEST_RIPE -> digest := f.data
			| AS_MSG_FIELD_TYPE_KEY -> begin
				let key_type = int_of_char f.data.[0] in 
				let value = Value.decode key_type ((Bytes.of_string f.data), 8, ((String.length f.data)-1)*8 ) in
				key := value
			end
			| _ -> ()
			end
		  ) fields;
		let key: Key.t = {namespace= !namespace; set= !set; digest= !digest; key= !key} in 
		key


	method add_namespace ns = 
		fields <- [ Field.namespace ns ] @ fields 		

	method add_set name = 
		fields <- [ Field.set name ] @ fields 

	method add_digest digest = 
		fields <- [ Field.digest digest ] @ fields 

	method add_user_key key = 
		fields <- [ Field.key key ] @ fields 

	method add_index_name name = 
		fields <- [ Field.index_name name ] @ fields

	method add_index_type t = 
		fields <- [ Field.index_type t ] @ fields

	method add_index_range filters =
		fields <- [ Field.index_range filters ] @ fields

	method add_scan_options priority = 
		fields <- [ Field.scan_options priority ] @ fields

	method add_query_binlist binlist = 
		fields <- [ Field.query_binlist binlist ] @ fields 

	method add_transaction_id id = 
		fields <- [ Field.transaction_id id ] @ fields

 	method header_size () = 
		Header.size

	method fields_size () = 
		List.fold_left (fun acc f -> acc + Field.size f) 0 fields

	method operations_size () = 
		List.fold_left (fun acc o -> acc + (Int32.to_int (Operation.size o))) 0 operations

 	method size () = 
		self#header_size () + self#fields_size () + self#operations_size () |> Int64.of_int
 
 	method encode () = 
 		header.n_fields <- List.length fields; 
 		header.n_opts <- List.length operations;
 		let header_b = Header.encode header in 
 		let fields_b = Bitstring.concat @@ List.map (fun f -> Field.encode f) (List.rev fields) in 
 		let operations_b = Bitstring.concat @@ List.map (fun o -> Operation.encode o) (List.rev operations) in 

 		let%bitstring b = {|
 			2				: 8  : int;
			3		 		: 8  : int;
			self#size () 	: 48 : bigendian;
 			header_b 		: Bitstring.bitstring_length header_b 		: bitstring;
 			fields_b 		: Bitstring.bitstring_length fields_b 		: bitstring;
 			operations_b 	: Bitstring.bitstring_length operations_b 	: bitstring
 		|} in 
 		b

	method decode (input: Bitstring.t) : Bitstring.t =
		match%bitstring input with
		|{|
			header_b		: 22 * 8 : bitstring;			
			remaining_bits  : -1 	: bitstring
		|} ->
			let buffer = ref remaining_bits in
			header <- Header.decode header_b;
 			for _ = 0 to header.n_fields-1 do
				let field, buff = Field.decode !buffer in
				Printf.printf "field:  %d  %s %d\n" (Field.Field_type.to_int field.field_type) field.data (int_of_char field.data.[0]);
				fields <- [field] @ fields;
				buffer := buff
			done;
 			for _ = 0 to header.n_opts-1 do 				
				let op, buff = Operation.decode !buffer in
				Printf.printf "op:  %d\n" (Operation.Operation_type.to_int op.op);
				(match op.bin_name with 
				| None -> ()
				| Some name -> Printf.printf "bin: %s\n" name);				
				Printf.printf "value: %s\n" @@ Value.to_str op.value;
				operations <- [op] @ operations;
				buffer := buff
			done;			
			!buffer

end


let decode_header input = 
	match%bitstring input with |{|
		2				: 8  : int;
		3		 		: 8  : int;
		msg_size 		: 48 : bigendian;
		remaining_bits  : -1 : bitstring
	|} -> (msg_size, remaining_bits)

