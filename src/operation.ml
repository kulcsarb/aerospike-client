exception InvalidOperationCode

module Operation_type = struct
	type t = 
		| AS_MSG_OP_READ		(* read the value in question *)
		| AS_MSG_OP_WRITE		(* write the value in question *)
		| AS_MSG_OP_INCR		(* arithmetically add a value to an existing value, works only on integers *)
		| AS_MSG_OP_APPEND		(* append a value to an existing value, works on strings and blobs *)
		| AS_MSG_OP_PREPEND		(* prepend a value to an existing value, works on strings and blobs *)
		| AS_MSG_OP_TOUCH 		(* touch a value without doing anything else to it - will increment the generation *)
		| AS_MSG_OP_MC_INCR		(* Memcache-compatible version of the increment command *)
		| AS_MSG_OP_MC_APPEND	(* append the value to an existing value, works only strings for now *)
		| AS_MSG_OP_MC_PREPEND	(* prepend a value to an existing value, works only strings for now *)
		| AS_MSG_OP_MC_TOUCH	(* Memcache-compatible touch - does not change generation *)

	let to_int ot = match ot with 
		| AS_MSG_OP_READ		-> 1 
		| AS_MSG_OP_WRITE		-> 2
		| AS_MSG_OP_INCR		-> 5 
		| AS_MSG_OP_APPEND		-> 9 
		| AS_MSG_OP_PREPEND		-> 10 
		| AS_MSG_OP_TOUCH 		-> 11 
		| AS_MSG_OP_MC_INCR		-> 129
		| AS_MSG_OP_MC_APPEND	-> 130
		| AS_MSG_OP_MC_PREPEND	-> 131
		| AS_MSG_OP_MC_TOUCH	-> 132

    let from_int = function
        | 1 -> AS_MSG_OP_READ
        | 2 -> AS_MSG_OP_WRITE
        | 5 -> AS_MSG_OP_INCR
        | 9 -> AS_MSG_OP_APPEND
        | 10 -> AS_MSG_OP_PREPEND 
        | 11 -> AS_MSG_OP_TOUCH
        | 129 -> AS_MSG_OP_MC_INCR
        | 130 -> AS_MSG_OP_MC_APPEND
        | 131 -> AS_MSG_OP_MC_PREPEND
        | 132 -> AS_MSG_OP_MC_TOUCH
    	| _ -> raise InvalidOperationCode

end

(* class operation ~op ?(bin_name=None) ?(value=None) = object(self)

	val mutable size : int32 = 0 

	method size = 
end
 *)


type t = {
	size: int32;
	op: Operation_type.t;	
	bin_name: string option;
	value: Value.t option
}

let data_size = function 
	| Some v -> Value.size_of v
	| None -> 0 

let bin_name_size = function 
	| Some name -> String.length name
	| None -> 0

let payload_size v n = (data_size v) + (bin_name_size n) + 4

let init ?(bin_name=None) ?(value: Value.t option = None) ~op = 
	{
		size = Int32.of_int (payload_size value bin_name);
		op;
		bin_name;
		value;
	}

let size o = Int32.(add o.size (of_int 4))

let encode o = 
	let operation_code = Operation_type.to_int o.op in
	let bin_name_length = bin_name_size o.bin_name in
	let data_size = data_size o.value in
	let bin_name = match o.bin_name with 
	| Some name -> name 
	| None -> ""
	in
	let data = match o.value with
	| Some value -> Value.encode value
	| None -> ""
	in
	let data_type = match o.value with
	| Some value -> Value.type_code_of value
	| None -> 0
	in
	let%bitstring b = 
	{|
		o.size						: 32 	: bigendian;
		operation_code			 	: 8 	: int;
		data_type 					: 8		: int;
		0							: 8		: int;
		bin_name_length 			: 8		: int;
		bin_name 					: bin_name_length * 8 : string;
		data 						: data_size * 8	: string		
	|} in b

let decode in_bits =
	match%bitstring in_bits with
	|{|
		size 			: 32 	: bigendian;
		op 				: 8		: int;
		bin_data_type 	: 8		: int;
		0 				: 8		: int;
		bin_name_length : 8		: int;
		bin_name 		: (bin_name_length*8): string;
		data 			: ((Int32.to_int size) - (bin_name_length + 4))*8: bitstring;
		leftover_bits	: -1	: bitstring
	|} -> let operation = {
				size; 
				op = Operation_type.from_int op;
				bin_name = Some bin_name;
				value = Some (Value.decode bin_data_type data)
			} in
		  (operation, leftover_bits)





let get ~bin_name =
	init ~op: AS_MSG_OP_READ ~bin_name: (Some bin_name) ~value: None

let put ~bin_name ~value = 
	init ~op: AS_MSG_OP_WRITE ~bin_name: (Some bin_name) ~value: (Some value)

let append ~bin_name ~value =
	init ~op: AS_MSG_OP_APPEND ~bin_name: (Some bin_name) ~value: (Some value)

let prepend ~bin_name ~value = 
	init ~op: AS_MSG_OP_PREPEND ~bin_name: (Some bin_name) ~value: (Some value)

(* stranegely enough, inc MUST have to send a value, even if that is outdated or completely irrelevant *)
let inc ~bin_name ~value =
	init ~op: AS_MSG_OP_INCR ~bin_name: (Some bin_name) ~value: (Some value)

let touch () = 
	init ~op: AS_MSG_OP_TOUCH ~bin_name: None ~value: None