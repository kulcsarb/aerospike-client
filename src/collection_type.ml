type t = [
	| `Unspecified
	| `List
	| `MapKeys
	| `MapValues
]

let to_int = function 
	| `List -> 1
	| `MapKeys -> 2
	| `MapValues -> 3
	| _ -> 0

let to_str = function 
	| `List -> "LIST"
	| `MapKeys -> "MAPKEYS"
	| `MapValues -> "MAPVALUES"
	| _ -> ""
