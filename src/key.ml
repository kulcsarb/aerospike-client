module Digest = Digestif.RMD160

type t = {
	namespace: string;
	set: string;
	digest: string;
	key: Value.t;
}

let compute_digest ?(set="") (key: Value.t) =
	let ctx = Digest.empty in 
	let ctx = match set with 
	| "" -> ctx
	| _ -> Digest.feed_string ctx set
	in 		
	let ctx = Digest.feed_string ctx @@ String.make 1 @@ char_of_int @@ Value.type_code_of key in
	let ctx = Digest.feed_string ctx @@ Value.encode key in
	ctx |> Digest.get |> Digest.to_raw_string


let make ?(namespace="") ?(set="") (key: Value.t) =
	let digest = compute_digest ~set key in
	{namespace; set; digest; key}


let to_fields ?(send_key=false)	t = 			
	begin match t.namespace with 
	| "" -> []
	| _ -> [Field.namespace t.namespace] 
	end
	@
	begin match t.set with 
	| "" -> []
	| _ -> [Field.set t.set]
	end
	@
	[ Field.digest t.digest ]
	@ 
	begin match send_key with
	| false -> []
	| true -> 
		[Field.key t.key]
	end	

let to_str k =
	Printf.sprintf "%s/%s:(%s)" k.namespace k.set (Value.to_str (Some k.key))