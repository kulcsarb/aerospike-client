exception InvalidFieldCode

module Field_type = struct
	type t = 
		| AS_MSG_FIELD_TYPE_NAMESPACE  (* namespace *)
		| AS_MSG_FIELD_TYPE_SET	(* a particular set within the namespace *)
		| AS_MSG_FIELD_TYPE_KEY	(* the key *)
		| AS_MSG_FIELD_TYPE_BIN	(* (Unused.) *)
		| AS_MSG_FIELD_TYPE_DIGEST_RIPE	(* the RIPEMD160 digest representing the key (20 bytes) *)
		| AS_MSG_FIELD_TYPE_GU_TID	(* (Unused.) *)
		| AS_MSG_FIELD_TYPE_DIGEST_RIPE_ARRAY	(* an array of digests *)
		| AS_MSG_FIELD_TYPE_TRID	(* transaction ID *)
		| AS_MSG_FIELD_TYPE_SCAN_OPTIONS	(* scan operation options *)
		| AS_MSG_FIELD_TYPE_INDEX_NAME	(* secondary index name *)
		| AS_MSG_FIELD_TYPE_INDEX_RANGE	(* secondary index query range *)
		| AS_MSG_FIELD_TYPE_INDEX_TYPE	(* secondary index type *)
		| AS_MSG_FIELD_TYPE_UDF_FILENAME	(* udf file name *)
		| AS_MSG_FIELD_TYPE_UDF_FUNCTION	(* udf function *)
		| AS_MSG_FIELD_TYPE_UDF_ARGLIST	(* udf argument list *)
		| AS_MSG_FIELD_TYPE_UDF_OP	(* udf operation type *)
		| AS_MSG_FIELD_TYPE_QUERY_BINLIST

	let to_int ft = 
	 match ft with 
		| AS_MSG_FIELD_TYPE_NAMESPACE -> 0
		| AS_MSG_FIELD_TYPE_SET	-> 1
		| AS_MSG_FIELD_TYPE_KEY	-> 2
		| AS_MSG_FIELD_TYPE_BIN	-> 3
		| AS_MSG_FIELD_TYPE_DIGEST_RIPE	-> 4
		| AS_MSG_FIELD_TYPE_GU_TID -> 5
		| AS_MSG_FIELD_TYPE_DIGEST_RIPE_ARRAY -> 6
		| AS_MSG_FIELD_TYPE_TRID -> 7
		| AS_MSG_FIELD_TYPE_SCAN_OPTIONS -> 8
		| AS_MSG_FIELD_TYPE_INDEX_NAME -> 21 
		| AS_MSG_FIELD_TYPE_INDEX_RANGE	-> 22
		| AS_MSG_FIELD_TYPE_INDEX_TYPE	-> 26 
		| AS_MSG_FIELD_TYPE_UDF_FILENAME -> 30
		| AS_MSG_FIELD_TYPE_UDF_FUNCTION -> 31
		| AS_MSG_FIELD_TYPE_UDF_ARGLIST	-> 32
		| AS_MSG_FIELD_TYPE_UDF_OP -> 33
		| AS_MSG_FIELD_TYPE_QUERY_BINLIST -> 40

    let from_int = function         
        | 0 -> AS_MSG_FIELD_TYPE_NAMESPACE
        | 1 -> AS_MSG_FIELD_TYPE_SET
        | 2 -> AS_MSG_FIELD_TYPE_KEY
        | 3 -> AS_MSG_FIELD_TYPE_BIN
        | 4 -> AS_MSG_FIELD_TYPE_DIGEST_RIPE
        | 5 -> AS_MSG_FIELD_TYPE_GU_TID
        | 6 -> AS_MSG_FIELD_TYPE_DIGEST_RIPE_ARRAY
        | 7 -> AS_MSG_FIELD_TYPE_TRID
        | 8 -> AS_MSG_FIELD_TYPE_SCAN_OPTIONS
        | 21 -> AS_MSG_FIELD_TYPE_INDEX_NAME 
        | 22 -> AS_MSG_FIELD_TYPE_INDEX_RANGE
        | 26 -> AS_MSG_FIELD_TYPE_INDEX_TYPE
        | 30 -> AS_MSG_FIELD_TYPE_UDF_FILENAME
        | 31 -> AS_MSG_FIELD_TYPE_UDF_FUNCTION
        | 32 -> AS_MSG_FIELD_TYPE_UDF_ARGLIST
        | 33 -> AS_MSG_FIELD_TYPE_UDF_OP
        | 40 -> AS_MSG_FIELD_TYPE_QUERY_BINLIST
        | _  -> raise InvalidFieldCode

end


type t = {
	size: int;
	field_type: Field_type.t;
	data: string;
}

let make field_type data =
	{
		size = (String.length data) + 1;
		field_type; 
		data
	}

let encode f = 
	let%bitstring bits = {|
		f.size |> Int32.of_int 				: 32 				: bigendian;
		f.field_type |> Field_type.to_int 	: 8 				: bigendian;
		f.data 								: (f.size-1) * 8 	: string
	|} in
	bits

let decode input =
	match%bitstring input with
	|{| 
		size 			: 32  	: bigendian;
		field_type 		: 8 	: map (fun v -> Field_type.from_int v);
		data 			: ((Int32.to_int size)-1)*8 : string;
		leftover_bits	: -1	: bitstring
	|} -> 
		let field = {size = Int32.to_int size; field_type; data} in
		(field, leftover_bits)
															
let size f = f.size + 4

let namespace ns = 
	make AS_MSG_FIELD_TYPE_NAMESPACE ns

let set s = 
	make AS_MSG_FIELD_TYPE_SET s

let key (k: Value.t) = 
	let code = String.make 1 @@ char_of_int @@ Value.type_code_of k in
	let data = Value.encode k in 
	make AS_MSG_FIELD_TYPE_KEY (code ^ data)

let digest digest =
	make AS_MSG_FIELD_TYPE_DIGEST_RIPE digest

let index_name name =
	make AS_MSG_FIELD_TYPE_INDEX_NAME name

let index_type t =
	make AS_MSG_FIELD_TYPE_INDEX_TYPE @@ String.make 1 @@ char_of_int t

let index_range filters = 
	let num = List.length filters in 	
	let encoded_filters = List.map (fun f -> Filter.encode f) filters in 
	let data = Bytes.concat Bytes.empty @@ List.map (fun (b, _, _) -> b) encoded_filters in
	let data = (String.make 1 @@ char_of_int num) ^ (Bytes.to_string data) in 
	make AS_MSG_FIELD_TYPE_INDEX_RANGE data

let scan_options priority =
	let data = Bytes.create 2 in 
	Bytes.set data 0 (char_of_int priority);
	Bytes.set data 1 (char_of_int 100);
	make AS_MSG_FIELD_TYPE_SCAN_OPTIONS (Bytes.to_string data)

let query_binlist bin_names =
	let num = List.length bin_names in 
	let names = List.map (fun name -> (String.make 1 (char_of_int (String.length name))) ^ name ) bin_names in 
	let encoded_names = String.concat "" names in 
	let data = (String.make 1 @@ char_of_int num) ^ encoded_names in
	make AS_MSG_FIELD_TYPE_QUERY_BINLIST data

let transaction_id id = 
	let%bitstring b = {| id : 64 : int |} in 
	let (b, _, _) = b in
	make AS_MSG_FIELD_TYPE_TRID (Bytes.to_string b)
