[@@@ocaml.warning "-36"]

exception InvalidMessageCode 
exception InvalidCollectionType
exception InvalidIndexType

class virtual command = object
	method virtual encode: unit -> Bitstring.t
	method virtual decode: Bitstring.t -> [`Ok | `Error]
end

class as_command = object(self)
	inherit command
	val message = new Message.message 
	val mutable response_header = Header.empty ()
	val mutable response_fields = []
	val mutable response_ops = [] 

	method header = message#header
	method message = message		

	method response_header = response_header
	method response_ops = response_ops
	method result_code = response_header.result_code 

	method build_message () = ()

	method encode () = 
		self#build_message ();
		message#encode ()

	method decode (reply: Bitstring.t) : [`Ok | `Error] = 
		match%bitstring reply with
		|{|
			2				: 8  : int;
			3		 		: 8  : int;
			msg_size 		: 48 : bigendian;
			header_b		: 22 * 8 : bitstring;
			fields_and_ops  : ((Int64.to_int msg_size) - 22) * 8 : bitstring
		|} ->
			let buffer = ref fields_and_ops in
			response_header <- Header.decode header_b;
			for _ = 0 to response_header.n_fields-1 do
				let field, buff = Field.decode !buffer in 
				response_fields <- [field] @ response_fields;
				buffer := buff
			done;
 			for _ = 0 to response_header.n_opts-1 do
				let op, buff = Operation.decode !buffer in
				response_ops <- [op] @ response_ops;
				buffer := buff
			done;
			match response_header.result_code with 
			| 0 -> `Ok
			| _ -> `Error

	method result () = 
		List.map (fun (op: Operation.t) -> 
			let name = match op.bin_name with 
			| Some bn -> bn				
			| None -> ""
			in 	
			(name, op.value)
		) response_ops;

end

class put key values = object
	inherit as_command as super	

	method! build_message () = 
		message#set_key key;
		List.iter (fun (bin, value) -> message#put bin value) values;	
			
end
 
class get key = object
	inherit as_command

	method! build_message () = 
		message#set_key key;		
		message#get ()

end

class exists key = object
	inherit as_command

	method! build_message () = 
		message#set_key key;		
		message#exists ()

end

class touch key = object
	inherit as_command

	method! build_message () = 
		message#set_key key;		
		message#touch ()

end

class delete key = object 
	inherit as_command
	
	method! build_message () =		
		message#set_key key;
		message#delete ()		

end


class append key bin value = object 
	inherit as_command
	
	method! build_message () =		
		message#set_key key;
		message#append bin value

end

class prepend key bin value = object 
	inherit as_command
	
	method! build_message () =		
		message#set_key key;
		message#prepend bin value

end


class query ?(bin_names=[]) namespace set_name  = object 
	inherit as_command 

(* 	
	""Filters determine query filters (Optional)
    Currently, only one filter is allowed by the server on a secondary index lookup.
    If multiple filters are necessary, see QueryFilter example for a workaround.
    QueryFilter demonstrates how to add additional filters in an user-defined
    aggregation function.""

    WTF?????
 *)	

	val mutable filters : Filter.t list = []
	val mutable responses = []
	val tr_id : int64 		= Random.int64 4611686018427387904L

	method add_filter f =
		filters <- [f] @ filters

	method! build_message () = 
		message#header.info1_read <- true;		
		(* message#header.info1_nobindata <- true; *)
		let scan_priority = 0 in 

		message#add_namespace namespace;
		(* message.add_index index_name  *)
		message#add_set set_name;

		begin match filters with 				
		| f :: _ ->
			(* add index type if specified *)
			if Collection_type.to_int f.col_type <> 0 then 			
				message#add_index_type @@ Collection_type.to_int f.col_type;

			(* encoding filters *)			
			message#add_index_range filters;
			
			(* bin names are concatenated, and added as a field *)
			message#add_query_binlist bin_names

		| [] ->
			(* no filters given, so this is a Scan operation *)
			message#add_scan_options scan_priority;	
			(* bin names are added as operations *)
			List.iter (fun name -> message#get_bin name) bin_names
		end;

		message#add_transaction_id tr_id

		(* TODO: add UDF function call here from query_command.rb *)

 	method! decode (bits: Bitstring.t) : [`Ok | `Error] = 
 		let _, bits = Message.decode_header bits in
		let rec _decode bits =
			let (_, offs, size) = bits in 
			if size <> 0 then begin
				Printf.printf "decoding message at: %d %d\n" offs size;
				let m = new Message.message in
				let bits = m#decode bits in 				
				if m#header.info3_last = false then
					responses <- [m] @ responses;				
					_decode bits
			end
		in		
		_decode bits;		
		`Ok

	method response () = 
		List.map (fun r ->  
			let key = r#get_key () in 
			let values = List.map (fun (o: Operation.t) -> (o.bin_name, o.value)) r#operations in 
			(key, values)
		) responses

end

class virtual info_command = object 
	inherit command 

	val mutable message = new Message.info 
	
    method decode bits : [`Ok | `Error] = 
    	message#decode bits;
    	`Ok

end


class truncate ?(set_name=None) ?(before_last_update=false) namespace = object(self)
	inherit info_command		

	method set_param = 
		match set_name with 
		| Some name -> Printf.sprintf ";set=%s" name
		| None -> ""
	
	method lut_param = 
		if before_last_update then
    		(* lut_nanos = (before_last_update.to_f * 1_000_000_000.0).round *)
    		(* str_cmd << ";lut=#{lut_nanos}" *)
    		""
    	else
    		";lut=now"  
    
    method encode () = 
		let cmd = Printf.sprintf "truncate:namespace=%s" namespace in 	
    	message#set_command (cmd ^ self#set_param ^ self#lut_param);
    	message#encode()

end


class create_index ?(collection_type=`Unspecified) namespace set_name index_name bin_name index_type = object
	inherit info_command

	method encode () = 

		let set_name_param = 
			match set_name with 
			| None -> ""
			| Some name -> ";set=" ^ name
		in
		let index_name_param = 
			Printf.sprintf ";indexname=%s;numbins=1" index_name
		in
		let collection_type_param = 			
			match collection_type with 
      		| `Unspecified -> ""
  			| `List -> ";indextype=LIST"
  			| `Mapkeys -> ";indextype=MAPKEYS"
  			| `Mapvalues -> ";indextype=MAPVALUES"  			
  			| _ -> raise InvalidCollectionType
      	in      	
      	let index_data_param = 
      		let it = match index_type with 
      			| `String -> "STRING"
      			| `Numeric -> "NUMERIC"
      			| _ -> raise InvalidIndexType
      		in 
      		Printf.sprintf ";indexdata=%s,%s" bin_name it
      	in
      	let index_cmd = "sindex-create:ns=" ^ namespace in 
      	let priority_param = ";priority=normal" in
	    let cmd = index_cmd ^ set_name_param ^ index_name_param ^ collection_type_param ^	index_data_param ^ priority_param in

    	message#set_command cmd;
    	message#encode()

end

class drop_index namespace set_name index_name = object
	inherit info_command

	method encode () = 
		let sname = match set_name with
		| None -> ""
		| Some name -> ";set=" ^ name 
		in 
		let cmd = Printf.sprintf "sindex-delete:ns=%s;%s;indexname=%s" namespace sname index_name in 
		message#set_command cmd;
		message#encode ()

end 
