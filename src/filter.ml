type t = {
	bin_name : string;
	begin_value : Value.t;
	end_value: Value.t;	
	col_type: Collection_type.t
}

let equal bin_name value = 
	{bin_name; begin_value=value; end_value=value; col_type=`Unspecified}

let contains bin_name value col_type = 
	{bin_name; begin_value=value; end_value=value; col_type}

let range bin_name begin_value end_value = 
	{bin_name; begin_value; end_value; col_type=`Unspecified}

let size c = 
	(String.length c.bin_name) + (Value.size_of c.begin_value) + (Value.size_of c.end_value) + 10

let encode c = 
	let bin_name		= c.bin_name in 
	let bin_name_size 	= String.length c.bin_name in 
	let val_type 		= Value.type_code_of c.begin_value in 
	let begin_value 	= Value.encode c.begin_value in 
	let begin_value_size = String.length begin_value in
	let end_value 		= Value.encode c.end_value in 
	let end_value_size 	= String.length end_value in 
	let%bitstring b = {|
		bin_name_size					: 8 				: int;
		bin_name						: bin_name_size * 8 : string;
		val_type						: 8 				: int;
		Int32.of_int begin_value_size	: 32 				: int;
		begin_value 					: begin_value_size * 8 : string;
		Int32.of_int end_value_size 	: 32 					: int;
		end_value 						: begin_value_size * 8 : string
	|} in 
	b
