type t = {
	header_sz 		  				: int;			(* number of bytes in this header (always equals 22 for protocol version 2.) *)
	mutable info1_read				: bool;
	mutable info1_get_all			: bool;
	mutable info1_batch				: bool;
	mutable info1_xdr				: bool;
	mutable info1_nobindata			: bool;
	mutable info1_level_b0			: bool;
	mutable info1_level_b1			: bool;
	mutable info2_write				: bool;
	mutable info2_delete			: bool;
	mutable info2_generation 		: bool;
	mutable info2_generation_gt		: bool;
	mutable info2_create_only 		: bool;
	mutable info2_bin_create_only 	: bool;
	mutable info2_respond_all_ops	: bool;
	mutable info3_last 				: bool;
	mutable info3_commit_level_b0	: bool;
	mutable info3_commit_level_b1	: bool;
	mutable info3_update_only		: bool;
	mutable info3_create_or_replace	: bool;
	mutable info3_replace_only 		: bool;
	mutable info3_bin_replace_only 	: bool;		
	mutable result_code 			: int;			(* on a response, whether the request succeeded or failed; 0 on requests *)
	mutable generation				: int32;		(* on request, apply this transaction only if the generation matches; on response, the current generation of this record *)
	mutable record_ttl				: int32;		(* on request, if > 0, set the expiration of this record to this number of seconds in the future; 0 means use the namespace default TTL; -1 means no expiration (use with caution) *)
	mutable transaction_ttl 		: int32;		(* on request, set transaction expiration time to this value; except on new batch request, this field stores the batch index *)
	mutable n_fields				: int;			(* number of fields to follow in the data payload, which will come first *)
	mutable n_opts					: int 			(* number of operations to follow in the data payload, which will follow the fields *)
}	

let size = 22

let empty () : t = 
	{
		header_sz 				= 22;
		info1_read 				= false;
		info1_get_all 			= false;
		info1_batch 			= false;
		info1_xdr 				= false;
		info1_nobindata 		= false;
		info1_level_b0 			= false;
		info1_level_b1 			= false;
		info2_write 			= false;
		info2_delete 			= false;
		info2_generation 		= false;
		info2_generation_gt	 	= false;
		info2_create_only 		= false;
		info2_bin_create_only 	= false;
		info2_respond_all_ops	= false;
		info3_last 				= false;
		info3_commit_level_b0	= false;
		info3_commit_level_b1	= false;
		info3_update_only		= false;
		info3_create_or_replace	= false;
		info3_replace_only 		= false;
		info3_bin_replace_only 	= false;
		result_code				= 0;
		generation 				= (Int32.of_int 0);
		record_ttl 				= (Int32.of_int 0);
		transaction_ttl 		= (Int32.of_int 0);
		n_fields 				= 0;
		n_opts 					= 0
	}

let encode (h: t) = 
	let%bitstring bits = {|
		h.header_sz						: 8 ;
		h.info1_level_b1				: 1 ;
		h.info1_level_b0				: 1 ;
		h.info1_nobindata 				: 1 ;
		h.info1_xdr						: 1 ;
		h.info1_batch					: 1 ;
		0								: 1 ;	
		h.info1_get_all					: 1 ;
		h.info1_read					: 1 ;
		h.info2_respond_all_ops			: 1 ;
		h.info2_bin_create_only			: 1 ;
		h.info2_create_only 			: 1 ;
		0								: 1 ;
		h.info2_generation_gt 			: 1 ;
		h.info2_generation 				: 1 ;	
		h.info2_delete					: 1 ;
		h.info2_write					: 1 ;
		0								: 1 ;
		h.info3_bin_replace_only		: 1 ;
		h.info3_replace_only 			: 1 ;
		h.info3_create_or_replace		: 1 ;
		h.info3_update_only 			: 1 ;
		h.info3_commit_level_b1			: 1 ;	
		h.info3_commit_level_b0			: 1 ;
		h.info3_last					: 1 ;								
		0								: 8 ;
		h.result_code 					: 8 ; 
		h.generation 					: 32 ;
		h.record_ttl 					: 32 ;
		h.transaction_ttl 				: 32 ;
		h.n_fields 						: 16 ;
		h.n_opts 						: 16
	|} in 
	bits

let decode in_bits = 
	match%bitstring in_bits with
	|{|
		header_sz						: 8;
		info1_level_b1					: 1 ;
		info1_level_b0					: 1 ;
		info1_nobindata 				: 1 ;
		info1_xdr						: 1 ;
		info1_batch						: 1 ;
		_								: 1 ;	
		info1_get_all					: 1 ;
		info1_read						: 1 ;
		info2_respond_all_ops			: 1 ;
		info2_bin_create_only			: 1 ;
		info2_create_only 				: 1 ;
		_								: 1 ;
		info2_generation_gt 			: 1 ;
		info2_generation 				: 1 ;	
		info2_delete					: 1 ;
		info2_write						: 1 ;
		_								: 1 ;
		info3_bin_replace_only			: 1 ;
		info3_replace_only 				: 1 ;
		info3_create_or_replace			: 1 ;
		info3_update_only 				: 1 ;
		info3_commit_level_b1			: 1 ;	
		info3_commit_level_b0			: 1 ;
		info3_last						: 1 ;			
		_ 								: 8;
		result_code						: 8;
		generation 						: 32;
		record_ttl						: 32;
		transaction_ttl					: 32;
		n_fields						: 16;
		n_opts							: 16
	|} -> 
		{
		header_sz; 
		info1_read;
		info1_get_all;
		info1_batch; 
		info1_xdr;
		info1_nobindata;
		info1_level_b0;
		info1_level_b1;
		info2_write;
		info2_delete;
		info2_generation; 
		info2_generation_gt;
		info2_create_only;
		info2_bin_create_only;
		info2_respond_all_ops;
		info3_last;
		info3_commit_level_b0;
		info3_commit_level_b1;
		info3_update_only;
		info3_create_or_replace;
		info3_replace_only;
		info3_bin_replace_only;
		result_code;
		generation;
		record_ttl;
		transaction_ttl;
		n_fields;
		n_opts;
		}	


