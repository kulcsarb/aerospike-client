
class connection address port = object(self)
	val addr = Unix.ADDR_INET((Unix.inet_addr_of_string address), port)
	val socket = Unix.socket PF_INET SOCK_STREAM 0

	method connect () = 
		Unix.connect socket addr;
		print_endline "Connected to server"

	(*Writes a bitstring to the specified socket, which we previously connected to*)
	method write data =
		print_endline "-------------    SENDING ----------------";
		Bitstring.hexdump_bitstring stdout data;
		let oc = Unix.out_channel_of_descr socket in
		Bitstring.bitstring_to_chan data oc;
		flush oc;
		print_endline "MSG sent."

	method read () : Bitstring.t =
		print_endline "waiting for reply";
		let buffer = Bytes.create 16384 in
		let size = Unix.read socket buffer 0 16384 in
		Printf.printf "Read %d bytes\n" size;
		let bits = (buffer, 0, size lsl 3) in
		print_endline "-------------    RECEIVED  ----------------";
		Bitstring.hexdump_bitstring stdout bits;	
		bits 

	method execute (command: Command.command) : [`Ok | `Error] = 
		self#write @@ command#encode ();
		self#read () |> command#decode

end

(* module Record = Map.Make(String)

let r = Record.empty 
let r = Record.add "name" (`Str "Mona") r
let r = Record.add "id" (`Int 1) r
 *)

let key = Key.make ~namespace: "test" ~set: "paygate" (`Str "Mona")

let put key (id: int) (name: string) (email: string) (addr: string) = 
	new Command.put key [("name", `Str name); ("id", `Int id); ("email", `Str email); ("city", `Str addr)]

let get key = 
	new Command.get key 

let delete key = 
	new Command.delete key 

let exist key = 
	new Command.exists key

let touch key = 
	new Command.touch key 

let append key =
	new Command.append key "name" (`Str "Hamd")

let prepend key =
	new Command.prepend key "name" (`Str "Mona")

let custom key = 
	let cmd = new Command.as_command in
	let message = cmd#message in 	
	message#set_key key;
	message#inc "id" (`Int 1);
	message#append "name" (`Str "Hamd");
	message#put "address" (`Str "Ajman");
	cmd

let seed_users conn = 
	let names = ["Fred"; "Bob"; "Laura"; "Sammy"; "Maude"; "Pete"; "Francisco"; "Daniel"; "Sarah"; "Nora"] in 
	let cities = ["Singapore"; "Boston"; "Hamburg"; "San Francisco"; "New Delhi"; "Tokyo"; "Sydney"; "Montreal"; "Istanbul"; "Vientiane"] in    
	for i = 0 to 10 do 
		let k = Key.make ~namespace: "test" ~set: "users" (`Str (Printf.sprintf "user%d" i)) in 
		let name = List.nth names (Random.int (List.length names)) in 
		let city = List.nth cities (Random.int (List.length cities)) in 
		let cmd = put k i name (name ^ "@gmail.com") city in 
		ignore (conn#execute (cmd :> Command.command))
	done


let (or) a b =
	match a with 
	| Some x -> x
	| None -> b 

let () = 
	print_endline "Connecting to Aerospike...";
	let conn = new connection "127.0.0.1" 3000 in
	conn#connect ();

	let k = Key.make ~namespace: "test" ~set: "users" (`Str (Printf.sprintf "user%d" 1)) in 
	let cmd = put k 1 "Balage" "gmail.com" "Budapest" in 
	Printf.printf "Size: %d\n" cmd#size; 
	ignore (conn#execute (cmd :> Command.command));

	print_endline "END."

(* 	seed_users conn;
	print_endline "---------------- CREATE INDEX -------------";
	let ci = new Command.create_index "test" (Some "users") "name_index" "name" `String in
	ignore (conn#execute (ci :> Command.command));

	let query = new Command.query "test" "users" ~bin_names: ["city"] in 
	query#add_filter @@ Filter.equal "name" (`Str "Bob");
	
	print_endline "---------------- EXECUTE QUERY -------------";
	ignore (conn#execute (query :> Command.command));

	let result = query#response () in 
	List.iter (fun (key, values) ->
		let key_str = Key.to_str key in
		let values_str = List.map (fun (bin_name, (value: Value.t option)) -> 
					Printf.sprintf "%s=%s" (bin_name or "") (Value.to_str value)
				) 
				values	in 
		let values_str = String.concat ", "  values_str in
		Printf.printf "key: %s | %s\n" key_str values_str		
	) result;
 *)
(* 	let ci = new Command.drop_index "test" (Some "users") "name_index" in
	ignore (conn#execute ci);
 *)	

	(* let cmd = get () in  *)
	(* let cmd = custom () in  *)

(* 	begin match conn#execute (cmd :> Command.command) with
	| `Ok -> Printf.printf "OK!\n"
	| `Error -> Printf.printf "ERROR!\n"
	end;	
	Printf.printf "Result code: %d\n" cmd#response_header.result_code;
	Printf.printf "Generation: %d\n" (Int32.to_int cmd#response_header.generation);
	Printf.printf "Record ttl: %d\n" (Int32.to_int cmd#response_header.record_ttl);
	Printf.printf "transaction_ttl: %d\n" (Int32.to_int cmd#response_header.transaction_ttl);
	Printf.printf "n_fields: %d\n" cmd#response_header.n_fields;
	Printf.printf "n_opts: %d\n" cmd#response_header.n_opts;

	List.iter (fun (bin, value) -> Printf.printf "%s = %s\n" bin (Value.to_str value)) (cmd#result ())
 *)
