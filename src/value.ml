exception InvalidDataType


type t = [
	| `Int of int
	| `Float of float
	| `Str of string	
	]

let type_code_of v = 
	match v with 
	| `Int _ -> 1
	| `Float _ -> 2
	| `Str _ -> 3		

let encode_float f : string = 		
	let i = f |> Int64.bits_of_float in 
	let%bitstring b = {| i 	: 64 : bigendian |} in 
	let b,_,_ = b in 
	b |> Bytes.to_string

let encode_integer i : string = 
	let%bitstring b = {| Int64.of_int i : 64 : bigendian |} in
	let b,_,_ = b in 
	b |> Bytes.to_string

let encode_string s : string = 
	s

let encode (v: t) =		
	match v with 
	| `Int i -> encode_integer i			
	| `Float f -> encode_float f			
	| `Str s -> encode_string s

let size_of (v: t) = 
	match v with 
	| `Int _ -> 8
	| `Float _ -> 8 
	| `Str s -> String.length s


let decode (data_type:int) (bits: Bitstring.t) : t = 
	let (_, _, length) = bits in 	
	match data_type with 
	| 1 -> begin 
		match%bitstring bits with |{| i : 64 : int |} -> (`Int (Int64.to_int i))
		end
	| 2 -> begin
		match%bitstring bits with |{| f : 64 : int |} -> (`Float (Int64.to_float f))
		end
	| 3 -> begin
		match%bitstring bits with |{| s : length : string |} -> (`Str s)
		end
	| _ -> 
		raise InvalidDataType


let to_str value = 
	match value with 
	| None -> "None"
	| Some v ->	
		match v with 
		| `Int i -> Printf.sprintf "Int %d" i
		| `Float f -> Printf.sprintf "Float %f" f
		| `Str s -> Printf.sprintf "Str %s" s
